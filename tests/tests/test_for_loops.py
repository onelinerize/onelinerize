def test_simple_for_loop(data):
    assert data["n"] == 10


def test_for_loop_else(data):
    assert data["n"] == 11


def test_for_loop_break_else(data):
    assert data["n"] == 10


def test_for_loop_continue_else(data):
    assert data["n"] == 11
