a = 7
b = 2
c = 5
c *= a
c /= b
c += a * 2
c -= b - c


class Meta(type):
    def __getattribute__(self, item):
        # this should not run
        global c
        c += 1
        return super().__getattribute__(item)


class Derived(metaclass=Meta):
    def __len__(self):
        # this should run only once
        return 5

    def __getattr__(self, item):
        # this should run only once
        global c
        c += 7
        return Derived()

    def __setitem__(self, key, value):
        # this should run only once
        global c
        c += 3

    def __getitem__(self, item):
        # this should run only once
        return c


c += len(Derived())
Derived().a[2] += 0
