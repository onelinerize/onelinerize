import ast
import logging
import os

import pytest

from oneline import transform

logging.basicConfig(level=logging.WARNING)


collect_ignore = ["oneline_tests"]


def pytest_generate_tests(metafunc):
    for fixture in metafunc.fixturenames:
        if fixture not in ("test", "data"):
            continue
        name = metafunc.function.__name__ + ".py"
        path = os.path.join(os.path.dirname(__file__), "oneline_tests", name)
        raw = open(path, "r").read()
        original = compile(raw, path, "exec")
        tree = ast.parse(raw)
        del raw
        tree = transform(tree, debug=True)
        metafunc.function.tree = tree
        code = compile(tree, path, "exec")
        if fixture == "data":
            original_data = {}
            code_data = {}
            exec(original, original_data)
            exec(code, code_data)
            metafunc.parametrize(fixture, [original_data, code_data])
        else:
            metafunc.parametrize(fixture, [original, code])


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    ret = outcome.get_result()
    if call.when == "call" and call.excinfo:
        ret.sections.append(("source", ast.unparse(item.function.tree)))
