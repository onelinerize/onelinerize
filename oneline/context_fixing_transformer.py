import ast
import typing

from .error_augmenting_transformer import ErrorAugmentingTransformer
from .location_preserving_transformer import LocationPreservingTransformer

T = typing.TypeVar("T", bound=ast.AST)


class ContextFixingTransformer(ErrorAugmentingTransformer, LocationPreservingTransformer):
    def __init__(self):
        super().__init__()
        self.context = ast.Load()

    def visit(self, node: typing.Optional[T]) -> typing.Optional[T]:
        new_fields = dict(ast.iter_fields(node))
        # noinspection PyProtectedMember
        if "ctx" in node._fields:
            new_fields["ctx"] = self.context
        else:
            self.context = ast.Load()  # all nodes that don't take context reset by default
        ret = type(node)(**new_fields)
        ret = self.fix_location(node, super().visit(ret))
        self.context = ast.Load()  # all nodes that do take context pass it only to their first child
        return ret

    def visit_Delete(self, node: ast.Delete) -> ast.Delete:
        targets = []
        for target in node.targets:
            self.context = ast.Del()
            targets.append(self.visit(target))
        return ast.Delete(targets)

    def visit_Assign(self, node: ast.Assign) -> ast.Assign:
        targets = []
        for target in node.targets:
            self.context = ast.Store()
            targets.append(self.visit(target))
        value = self.visit(node.target)
        type_comment = self.visit(node.type_comment)
        return ast.Assign(targets, value, type_comment)

    def visit_AugAssign(self, node: ast.AugAssign) -> ast.AugAssign:
        self.context = ast.Store()
        target = self.visit(node.target)
        op = self.visit(node.op)
        value = self.visit(node.value)
        return ast.AugAssign(target, op, value)

    def visit_AnnAssign(self, node: ast.AnnAssign) -> ast.AnnAssign:
        self.context = ast.Store()
        target = self.visit(node.target)
        annotation = self.visit(node.annotation)
        value = self.visit(node.value) if node.value is not None else None
        return ast.AnnAssign(target, annotation, value, node.simple)

    def visit_For(self, node: ast.For) -> ast.For:
        self.context = ast.Store()
        target = self.visit(node.target)
        iterator = self.visit(node.iter)
        body = [self.visit(expr) for expr in node.body]
        orelse = [self.visit(expr) for expr in node.orelse]
        return ast.For(target, iterator, body, orelse, node.type_comment)

    def visit_AsyncFor(self, node: ast.AsyncFor) -> ast.AsyncFor:
        self.context = ast.Store()
        target = self.visit(node.target)
        iterator = self.visit(node.iter)
        body = [self.visit(expr) for expr in node.body]
        orelse = [self.visit(expr) for expr in node.orelse]
        return ast.AsyncFor(target, iterator, body, orelse, node.type_comment)

    def visit_NamedExpr(self, node: ast.NamedExpr) -> ast.NamedExpr:
        self.context = ast.Store()
        target = self.visit(node.target)
        value = self.visit(node.value)
        return ast.NamedExpr(target, value)

    def visit_comprehension(self, node: ast.comprehension) -> ast.comprehension:
        self.context = ast.Store()
        target = self.visit(node.target)
        iterator = self.visit(node.iter)
        ifs = [self.visit(if_exp) for if_exp in node.ifs]
        return ast.comprehension(target, iterator, ifs, node.is_async)

    def visit_ExceptHandler(self, node: ast.ExceptHandler) -> ast.ExceptHandler:
        type_expr = self.visit(node.type)
        if node.name is not None:
            self.context = ast.Store()
            name = self.visit(node.name)
        else:
            name = None
        body = [self.visit(expr) for expr in node.body]
        return ast.ExceptHandler(type_expr, name, body)

    def visit_withitem(self, node: ast.withitem) -> ast.withitem:
        context_expr = self.visit(node.context_expr)
        if node.asname is not None:
            self.context = ast.Store()
            asname = self.visit(node.asname)
        else:
            asname = None
        return ast.withitem(context_expr, asname)

    def visit_Attribute(self, node: ast.Attribute) -> ast.Attribute:
        self.context = ast.Load()
        return self.generic_visit(node)

    def visit_Subscript(self, node: ast.Subscript) -> ast.Subscript:
        self.context = ast.Load()
        return self.generic_visit(node)

    # preserve context between children

    def visit_Tuple(self, node: ast.Tuple) -> ast.Tuple:
        context = self.context
        elts = []
        for elt in node.elts:
            self.context = context
            elts.append(self.visit(elt))
        return ast.Tuple(elts, context)

    def visit_List(self, node: ast.List) -> ast.List:
        context = self.context
        elts = []
        for elt in node.elts:
            self.context = context
            elts.append(self.visit(elt))
        return ast.List(elts, context)
