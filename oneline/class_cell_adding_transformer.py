import ast

from .error_augmenting_transformer import ErrorAugmentingTransformer
from .location_preserving_transformer import LocationPreservingTransformer
from .utils import _wrap_listcomp


class ClassCellAddingTransformer(ErrorAugmentingTransformer, LocationPreservingTransformer):
    def __init__(self, ctx, instantiated):
        super().__init__()
        self.ctx = ctx
        self.instantiated = instantiated

    def visit_Lambda(self, node: ast.Lambda) -> ast.Lambda:
        return ast.Lambda(node.args, ast.Subscript(
            _wrap_listcomp(node.body, [ast.Name("__class__", None)], self.instantiated),
            ast.Constant(0, None),
            None
        ))
