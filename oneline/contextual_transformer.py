import ast
import typing

from ast_scope.scope import Scope

from .context import Context
from .stop_condition import StopCondition

T = typing.TypeVar("T", bound=ast.AST)


class ContextualTransformer(ast.NodeTransformer):
    def __init__(self, ctx: Context):
        super().__init__()
        self.ctx = ctx
        self.scope: list[Scope] = [ctx.global_scope]
        self.stop_tests: list[list[StopCondition]] = [[]]

    def visit(self, node: typing.Optional[T]) -> typing.Optional[T]:
        scope = self.ctx.scope_from_node(node)
        if scope and scope not in self.scope:
            self.scope.append(scope)
            self.stop_tests.append([])
        else:
            scope = False
        ret = super().visit(node)
        if scope:
            self.scope.pop()
            self.stop_tests.pop()
        return ret
