class TempScope:
    _COUNTER = 0

    def __init__(self):
        self.counter = self._COUNTER
        type(self)._COUNTER += 1
