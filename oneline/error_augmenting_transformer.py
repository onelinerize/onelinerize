import ast
import typing

from .exceptions import ErrorWithLocation


T = typing.TypeVar("T", bound=ast.AST)


class ErrorAugmentingTransformer(ast.NodeTransformer):
    def __init__(self, *args, **kwargs):
        self.stack = []
        super().__init__(*args, **kwargs)

    def visit(self, node: typing.Optional[T]) -> typing.Optional[T]:
        self.stack.append(node)
        try:
            ret = super().visit(node)
        except ErrorWithLocation:
            raise
        except Exception as e:
            raise ErrorWithLocation(node, self.stack) from e
        self.stack.pop()
        return ret

    def generic_visit(self, node: typing.Optional[T]) -> typing.Optional[T]:
        if node is None:
            return node
        if not isinstance(node, ast.AST):
            raise ErrorWithLocation(self.stack[-1], self.stack) from TypeError(f"tried to visit {node}, expected ast.AST")
        return super().generic_visit(node)
