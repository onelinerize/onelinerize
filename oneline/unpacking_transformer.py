import ast
import typing


T = typing.TypeVar("T", bound=ast.AST)


class UnpackingTransformer(ast.NodeTransformer):
    def visit(self, node: typing.Optional[T]) -> typing.Optional[T]:
        return super().visit(node)

    def generic_visit(self, node: typing.Optional[T]) -> typing.Optional[T]:
        fields = ast.iter_fields(node)
        new_fields = {}
        for field, _ in fields:
            new_fields[field] = self.unpack_visit(node, field)
        return type(node)(**new_fields)

    def unpack_visit(self, node: T, field: str):
        value = getattr(node, field)
        if isinstance(node, ast.Constant) and field == "value":
            return value
        elif isinstance(value, list):
            ret = []
            for subnode in value:
                if subnode is not None:
                    assert isinstance(subnode, ast.AST), (field, value, subnode)
                    subret = self.visit(subnode)
                else:
                    subret = None
                if isinstance(subret, list):
                    ret += subret
                else:
                    ret.append(subret)
            return ret
        elif isinstance(value, ast.AST):
            ret = self.visit(value)
            assert not isinstance(ret, list) and ret is not None, ret
            return ret
        else:
            return value
