import ast
import typing

from ast_scope.scope import FunctionScope

from .context import Context
from .error_augmenting_transformer import ErrorAugmentingTransformer
from .location_preserving_transformer import LocationPreservingTransformer

T = typing.TypeVar("T", bound=ast.AST)


class FirstArgPreserverTransformer(ErrorAugmentingTransformer, LocationPreservingTransformer):
    def __init__(self, ctx: Context, requires_self: dict[ast.AST, FunctionScope]):
        super().__init__()
        self.ctx = ctx
        self.requires_self = requires_self
        self.found: list[FunctionScope] = []

    def generic_visit(self, node: T) -> T:
        found = self.requires_self.get(node, None)
        if found:
            self.found.append(found)
        return super().generic_visit(node)

    def visit_Lambda(self, node: ast.Lambda) -> ast.Lambda:
        count = len(self.found)
        ret = self.generic_visit(node)
        if len(self.found) > count:
            # ensure that all found nodes have the same self
            assert all(self.found[count] == self.found[i] for i in range(count + 1, len(self.found))), (self.found, count)
            found = self.found[count]
            del self.found[count:]
            first_arg = (found.function_node.args.posonlyargs + found.function_node.args.args)[0]
            return ast.Lambda(node.args, ast.Call(ast.Lambda(ast.arguments([], [first_arg], None, [], [], None, []), node.body), [self.ctx.var(first_arg.arg, found)], []))
        return ret
