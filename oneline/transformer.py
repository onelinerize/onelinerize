import ast
import uuid

from .context import Context
from .context_fixing_transformer import ContextFixingTransformer
from .first_arg_preserver_transformer import FirstArgPreserverTransformer
from .main_transformer import MainTransformer


# PY-29435
# noinspection PyUnreachableCode
def transform(tree: ast.Module, *args, **kwargs):
    ctx = Context(tree, *args, **kwargs)
    main_transformer = MainTransformer(ctx)
    tree = main_transformer.visit(tree)
    tree = FirstArgPreserverTransformer(ctx, main_transformer.requires_self).visit(tree)
    tree = ContextFixingTransformer().visit(tree)
    tree = ast.fix_missing_locations(tree)
    if __debug__:
        # noinspection PyBroadException
        try:
            compile(tree, "", "exec")
        except Exception:
            import difflib
            import html
            import logging
            import os
            from .location_removing_transformer import LocationRemovingTransformer
            from .convert_negatives_transformer import ConvertNegativesTransformer
            bad_tree = LocationRemovingTransformer().visit(ConvertNegativesTransformer().visit(tree))
            try:
                # noinspection PyUnresolvedReferences
                import astpretty
            except ImportError:
                dump = lambda t: ast.dump(t).split(",")
            else:
                dump = lambda t: astpretty.pformat(t).splitlines()
            try:
                code = ast.unparse(tree)
                good_tree = LocationRemovingTransformer().visit(ast.parse(code))
            except Exception:
                open("dump", "w").write("\n".join(dump(tree)))
                raise
            print(good_tree.body[0].lineno, bad_tree.body[0].lineno)
            print(dump(bad_tree)[4])
            print(dump(good_tree)[4])
            bad_text = [html.escape(line).encode("utf-8", "xmlcharrefreplace").decode("utf-8") for line in dump(bad_tree)]
            good_text = [html.escape(line).encode("utf-8", "xmlcharrefreplace").decode("utf-8") for line in dump(good_tree)]
            path = os.path.abspath(f"astdiff{uuid.uuid4()}.html")
            with open(path, "w") as f:
                f.write(difflib.HtmlDiff().make_file(good_text, bad_text, "Good AST", "Bad AST"))
            logging.warning(f"Exception occured while compiling AST. Stored diff in {path}.\n{code}", exc_info=True)
    return tree
