import ast

from .error_augmenting_transformer import ErrorAugmentingTransformer
from .location_preserving_transformer import LocationPreservingTransformer


class ConvertNegativesTransformer(ErrorAugmentingTransformer, LocationPreservingTransformer):
    """
    Change Constant(-1) to UnaryOp(USub(), 1)

    Normally this class is not needed.
    However, it is very useful when diffing the results of ast.unparse with the generated AST to find a bug
    """

    def visit_Constant(self, node: ast.Constant) -> ast.AST:
        if isinstance(node.value, int) and node.value < 0:
            return ast.UnaryOp(ast.USub(), ast.Constant(abs(node.value), node.kind))
        return node
