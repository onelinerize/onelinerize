import ast
import typing

from ast_scope.scope import Scope

from oneline.temp_scope import TempScope

BLANK_ARGUMENTS = ast.arguments([], [], None, [], [], None, [])

ScopeType = typing.Union[Scope, TempScope]
ScopeLike = typing.Union[ScopeType, ast.AST]


def args_kwargs_arguments(ctx, scope: ScopeLike):
    return ast.arguments(
        [],
        [],
        ast.arg(ctx.scope_name(scope, "args"), None, None),
        [],
        [],
        ast.arg(ctx.scope_name(scope, "kwargs"), None, None),
        []
    )


def _wrap_listcomp(inner_body: ast.AST, targets: list[ast.AST], values: ast.AST, subscript: bool = False):
    ret = ast.ListComp(
        inner_body,
        [ast.comprehension(
            ast.Tuple(targets),
            ast.Tuple([ast.BinOp(ast.Tuple([values]), ast.Mult(), ast.Constant(len(targets), None))]),
            [],
            0
        )]
    )
    if subscript:
        ret = ast.Subscript(ret, ast.Constant(0, None), None)
    return ret


def _cache_supporting_assign(node: ast.AST):
    if isinstance(node, ast.Attribute):
        return node.value, lambda cache: ast.Attribute(cache, node.attr, node.ctx)
    if isinstance(node, ast.Subscript):
        return node.value, lambda cache: ast.Subscript(cache, node.slice, node.ctx)
    if isinstance(node, ast.Name):
        return node, lambda cache: node
