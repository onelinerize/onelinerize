import ast
import typing

from .class_cell_adding_transformer import ClassCellAddingTransformer
from .context import Context
from .contextual_transformer import ContextualTransformer
from .error_augmenting_transformer import ErrorAugmentingTransformer
from .location_preserving_transformer import LocationPreservingTransformer
from .stop_condition import StopConditionAll, StopCause, StopConditionContinue, StopConditionBreak, \
    StopConditionLoopMarker, StopConditionIf, StopConditionTry
from .temp_scope import TempScope
from .unpacking_transformer import UnpackingTransformer
from .utils import BLANK_ARGUMENTS, ScopeLike, _wrap_listcomp, _cache_supporting_assign


class MainTransformer(
    ErrorAugmentingTransformer,
    LocationPreservingTransformer,
    ContextualTransformer,
    UnpackingTransformer
):
    def __init__(self, ctx: Context):
        super().__init__(ctx)
        self.requires_self = {}

    def visit_Module(self, node: ast.Module) -> ast.AST:
        ret = self.generic_visit(node)
        ret.body = [ast.Expr(ast.Tuple(
            self.ctx.init_scope(self.scope[-1]) + ret.body + self.ctx.store_scope(self.scope[-1])
        ))]
        return ret

    def visit_Expr(self, node: ast.Expr) -> ast.AST:
        return self.visit(node.value)

    visit_Global = visit_Nonlocal = lambda self, node: []

    def visit_Name(self, node: ast.Name) -> ast.AST:
        ret = self.ctx.var(node)
        if isinstance(node.ctx, ast.Load):
            scope = self.ctx.scope_from_node(node)
            inner_scope = TempScope()
            error_type = self.ctx.get_builtin("NameError" if scope is self.ctx.global_scope else "UnboundLocalError")
            error_str = ast.Constant(
                f"name {node.id!r} is not defined"
                if scope is self.ctx.global_scope else
                f"local variable {node.id!r} referenced before assignment",
                None
            )
            ret = ast.Subscript(_wrap_listcomp(
                ast.IfExp(
                    ast.Compare(self.ctx.scope(inner_scope, "val"), [ast.Is()], [self.ctx.sentinel()]),
                    self._raise(ast.Call(error_type, [error_str], [])),
                    self.ctx.scope(inner_scope, "val")
                ),
                [self.ctx.scope(inner_scope, "val")],
                ast.Call(
                    ast.Attribute(self.ctx.scope(node, "data"), "get", None),
                    [ast.Constant(node.id, None), self.ctx.sentinel()],
                    []
                )
            ), ast.Constant(0, None), None)
            if node.id == "super":
                # Special case from Python - we need the __class__ cellvar and the self local
                # Ensure that the parent lambda takes and repasses self from cellvar to local
                ret = ast.Subscript(
                    ast.Tuple([ast.Name("__class__", None), ret]),
                    ast.Constant(1, None),
                    node.ctx
                )
                self.requires_self[ret] = self.scope[-1]
        elif isinstance(node.ctx, ast.Del):
            return node
        return ret

    def visit_Assign(self, node: ast.Assign) -> ast.AST:
        ret = self.generic_visit(node)
        return _wrap_listcomp(ast.Constant(None, None), ret.targets, ret.value)

    binop = {
        "Add": "__iadd__",
        "Sub": "__isub__",
        "Mult": "__imul__",
        "MatMult": "__imatmul__",
        "Div": "__itruediv__",
        "Mod": "__imod__",
        "LShift": "__ilshift__",
        "RShift": "__irshift__",
        "BitOr": "__ior__",
        "BitXor": "__ixor__",
        "BitAnd": "__iand__",
        "FloorDiv": "__ifloordiv__",
        "Pow": "__ipow__",
    }

    def visit_AugAssign(self, node: ast.AugAssign) -> list[ast.AST]:
        inner_scope = TempScope()
        ret = self.generic_visit(node)
        cache, target = _cache_supporting_assign(ret.target)
        target = target(self.ctx.scope(inner_scope, "target_left"))
        return [_wrap_listcomp(_wrap_listcomp(
            self._try(
                [ast.Call(
                    ast.Attribute(self.ctx.scope(inner_scope, "mm"), "append", None),
                    [ast.Call(
                        ast.Attribute(self.ctx.get_builtin("object"), "__getattribute__", None),
                        [
                            ast.Call(self.ctx.get_builtin("type"), [self.ctx.scope(inner_scope, "target")], []),
                            ast.Constant(self.binop[type(ret.op).__name__], None)
                        ],
                        []
                    )],
                    []
                )],
                [(
                    self.ctx.get_builtin("AttributeError"),
                    None,
                    [_wrap_listcomp(
                        ast.Constant(None, None),
                        [target],
                        ast.BinOp(self.ctx.scope(inner_scope, "target"), ret.op, ret.value)
                    )]
                )],
                [_wrap_listcomp(
                    ast.Constant(None, None),
                    [target],
                    ast.Call(
                        ast.Subscript(self.ctx.scope(inner_scope, "mm"), ast.Constant(0, None), None),
                        [self.ctx.scope(inner_scope, "target"), ret.value],
                        []
                    )
                )],
                None
            ),
            [ast.Tuple([
                self.ctx.scope(inner_scope, "mm"),
                self.ctx.scope(inner_scope, "target")
            ])],
            ast.Tuple([ast.List([]), target])
        ), [self.ctx.scope(inner_scope, "target_left")], cache)]

    def visit_AnnAssign(self, node: ast.AnnAssign) -> None:
        return None

    def visit_arg(self, node: ast.arg) -> ast.AST:
        return ast.arg(node.arg, None, node.type_comment)

    def visit_Assert(self, node: ast.Assert) -> ast.AST:
        ret = self.generic_visit(node)
        return ast.IfExp(
            ast.BoolOp(ast.And(), [self.ctx.get_builtin("__debug__"), ast.UnaryOp(ast.Not(), ret.test)]),
            self._raise(ast.Call(self.ctx.get_builtin("AssertionError"), [ret.msg] if ret.msg else [], [])),
            ast.Constant(None, None)
        )

    def visit_Delete(self, node: ast.Delete) -> list[ast.AST]:
        return self._delete(self.generic_visit(node).targets)

    def _delete(self, targets: list[ast.AST], strict: bool = True):
        inner_scope = TempScope()
        ret = []
        catch = None
        for target in targets:
            if isinstance(target, ast.Attribute):
                mm = "__delattr__"
                target_left = target.value
                target_right = ast.Constant(target.attr, None)
            elif isinstance(target, ast.Subscript):
                mm = "__delitem__"
                target_left = target.value
                target_right = target.slice
            elif isinstance(target, ast.Name):
                mm = "__delitem__"
                target_left = self.ctx.scope(target, "data")
                target_right = ast.Constant(target.id, None)
                scope = self.ctx.scope_from_node(target)
                catch = (  # TODO commonize this code with visit_Name
                    "NameError" if scope is self.ctx.global_scope else "UnboundLocalError",
                    ast.Constant(
                        f"name {target.id!r} is not defined"
                        if scope is self.ctx.global_scope else
                        f"local variable {target.id!r} referenced before assignment",
                        None
                    )
                )
            else:
                raise SyntaxError("unsupported delete target")
            if strict:
                inner_ret = _wrap_listcomp(ast.Call(
                    ast.Call(
                        ast.Attribute(self.ctx.get_builtin("object"), "__getattribute__", None),
                        [ast.Call(
                            self.ctx.get_builtin("type"),
                            [self.ctx.scope(inner_scope, "value")],
                            []
                        ), ast.Constant(mm, None)], []
                    ),
                    [self.ctx.scope(inner_scope, "value"), target_right],
                    []
                ), [self.ctx.scope(inner_scope, "value")], target_left)
            else:
                inner_ret = ast.Call(
                    ast.Attribute(
                        target_left,
                        mm,
                        None
                    ),
                    [target_right],
                    []
                )
            if catch:
                inner_ret = self._rewrite_errors(inner_ret, True, KeyError=catch)
            ret.append(inner_ret)
        return ret

    def visit_Import(self, node: ast.Import) -> list[ast.AST]:
        return [
            self.ctx.assign_var_in_scope(
                self.scope[-1],
                name.asname or name.name,
                ast.Call(
                    self.ctx.get_builtin("__import__"),
                    [
                        ast.Constant(name.name, None),
                        self.ctx.scope(self.ctx.global_scope, "data"),
                        self.ctx.scope(self.scope[-1], "data")
                    ], []
                )
            )
            for name in node.names
        ]

    def visit_ImportFrom(self, node: ast.ImportFrom) -> ast.AST:
        scope = TempScope()
        return _wrap_listcomp(
            ast.Tuple([
                self.ctx.assign_var_in_scope(
                    self.scope[-1],
                    name.asname or name.name,
                    ast.Attribute(self.ctx.scope(scope, "import"), name.name, None)
                )
                for name in node.names
            ]),
            [self.ctx.scope(scope, "import")],
            ast.Call(
                self.ctx.get_builtin("__import__"),
                [
                    ast.Constant(node.module, None),
                    self.ctx.scope(self.ctx.global_scope, "data"),
                    self.ctx.scope(self.scope[-1], "data"),
                    ast.List([ast.Constant(name.name, None) for name in node.names], None),
                    ast.Constant(node.level, None)
                ], []
            )
        )

    def visit_If(self, node: ast.If) -> ast.AST:
        inner_scope = TempScope()
        self.stop_tests[-1].append(StopConditionIf(inner_scope, "stop"))
        ret = self.generic_visit(node)
        self.stop_tests[-1].pop()
        inner_body = ast.Tuple([
            ast.Lambda(BLANK_ARGUMENTS, expr) for expr in ret.body
        ])
        wrapped_body = self._wrap_function(inner_body, 0, inner_scope, "stop", None, False)
        return ast.IfExp(ret.test, wrapped_body, ast.List(ret.orelse or [ast.Constant(None, None)]))

    def visit_For(self, node: ast.For) -> ast.AST:
        inner_scope = TempScope()
        target = self.unpack_visit(node, "target")
        iter_expr = self.unpack_visit(node, "iter")
        self.stop_tests[-1].append(StopConditionLoopMarker(inner_scope, None))
        self.stop_tests[-1].append(StopConditionBreak(inner_scope, "break"))
        self.stop_tests[-1].append(StopConditionContinue(inner_scope, "continue"))
        body = self.unpack_visit(node, "body")
        self.stop_tests[-1].pop()
        self.stop_tests[-1].pop()
        self.stop_tests[-1].pop()
        self.stop_tests[-1].append(StopConditionAll(inner_scope, "stop"))
        orelse = self.unpack_visit(node, "orelse")
        self.stop_tests[-1].pop()
        inner_body = ast.Tuple([
            ast.Lambda(BLANK_ARGUMENTS, expr) for expr in body
        ])
        return _wrap_listcomp(
            ast.Tuple([
                self._wrap_loop(inner_scope, inner_body, iter_expr, target, ast.Constant(True, None)),
                *((ast.IfExp(
                    self.ctx.scope(inner_scope, "break"),
                    ast.Constant(None, None),
                    self._wrap_function(
                        ast.Tuple([ast.Lambda(BLANK_ARGUMENTS, expr) for expr in orelse]),
                        0,
                        inner_scope,
                        data_type=None,
                        subscript=False
                    )),) if orelse else ())
            ]),
            [self.ctx.scope(inner_scope, "break")],
            ast.List([])
        )

    def visit_While(self, node: ast.While) -> list[ast.AST]:
        inner_scope = TempScope()
        test = self.unpack_visit(node, "test")
        self.stop_tests[-1].append(StopConditionLoopMarker(inner_scope, None))
        self.stop_tests[-1].append(StopConditionBreak(inner_scope, "break"))
        self.stop_tests[-1].append(StopConditionContinue(inner_scope, "continue"))
        body = self.unpack_visit(node, "body")
        self.stop_tests[-1].pop()
        self.stop_tests[-1].pop()
        self.stop_tests[-1].pop()
        self.stop_tests[-1].append(StopConditionAll(inner_scope, "stop"))
        orelse = self.unpack_visit(node, "orelse")
        self.stop_tests[-1].pop()
        inner_body = ast.Tuple([
            ast.Lambda(BLANK_ARGUMENTS, expr) for expr in body
        ])
        wrapped_body = self._wrap_loop(
            inner_scope,
            inner_body,
            self._infinite_iter(ast.BoolOp(
                ast.Or(),
                [ast.UnaryOp(ast.Not(), test), self.ctx.scope(inner_scope, "break")]
            )),
            ast.Name(self.ctx.writeonly(), None),
            None
        )
        return _wrap_listcomp(ast.Tuple([wrapped_body, *((ast.IfExp(
            self.ctx.scope(inner_scope, "break"),
            ast.Constant(None, None),
            self._wrap_function(
                ast.Tuple([ast.Lambda(BLANK_ARGUMENTS, expr) for expr in orelse]),
                0,
                inner_scope,
                data_type=None,
                subscript=False
            )),) if orelse else ())
        ]), [self.ctx.scope(inner_scope, "break")], ast.List([]))

    def visit_Break(self, node: ast.Break) -> ast.AST:
        return self._stop(StopCause.BREAK)

    def visit_Continue(self, node: typing.Optional[ast.Continue]) -> ast.AST:
        return self._stop(StopCause.CONTINUE)

    def _stop(self, cause: StopCause, value: ast.AST = ast.Constant(None, None)) -> ast.AST:
        ret = [ast.Constant(None, None)]
        for test in reversed(self.stop_tests[-1]):
            if test.stop_handling(cause):
                break
            if test.handle(cause):
                if test.STORE_VALUE:
                    ret.append(ast.Call(ast.Attribute(test.var(self.ctx), "append", None), [value], []))
                else:
                    ret[-1] = ast.Call(ast.Attribute(test.var(self.ctx), "append", None), [ret[-1]], [])
        if isinstance(ret[0], ast.Constant):
            del ret[0]
        if not ret:
            raise SyntaxError(f"return/break/continue outside function/loop")
        return ast.Tuple(ret) if len(ret) > 1 else ret[0]

    def visit_Pass(self, node: ast.Pass) -> ast.AST:
        return ast.Constant(None, None)

    def visit_Try(self, node: ast.Try) -> ast.AST:
        inner_scope = TempScope()
        self.stop_tests[-1].append(StopConditionTry(inner_scope, "stop"))
        ret = self.generic_visit(node)
        self.stop_tests[-1].pop()
        handlers = []  # type, name, body
        for i, handler in enumerate(ret.handlers):
            handlers.append((
                handler.type,
                self.ctx.var(handler.name, node.handlers[i]) if handler.name else None,
                handler.body
            ))
        return self._try(ret.body, handlers, ret.orelse, ret.finalbody)

    def _try(
            self,
            body: list[ast.AST],
            handlers: list[tuple[typing.Optional[ast.AST], typing.Optional[ast.AST], list[ast.AST]]],
            orelse: typing.Optional[list[ast.AST]],
            finalbody: typing.Optional[list[ast.AST]]
    ):
        # each handler is [catch_type, store_target, body]
        inner_scope = TempScope()
        inner_body = ast.Tuple([
            ast.Lambda(BLANK_ARGUMENTS, expr) for expr in body
        ])
        wrapped_body = self._wrap_function(inner_body, 0, inner_scope, "stop", None, False)
        body = ast.Call(ast.Call(ast.Call(ast.Call(self.ctx.get_builtin("type"), [
            ast.Constant("", None),
            ast.Tuple([ast.Attribute(ast.Call(
                self.ctx.get_builtin("__import__"),
                [ast.Constant("contextlib", None)], []
            ), "ContextDecorator", None)]),
            ast.Dict([ast.Constant("__enter__", None), ast.Constant("__exit__", None)], [
                ast.Lambda(
                    ast.arguments([], [ast.arg(self.ctx.writeonly(), None)], None, [], [], None, []),
                    ast.Constant(None, None)
                ),
                ast.Lambda(
                    ast.arguments(
                        [],
                        [ast.arg(self.ctx.writeonly(), None)],
                        ast.arg(self.ctx.scope_name(inner_scope, "exc"), None),
                        [],
                        [],
                        None,
                        []
                    ),
                    ast.UnaryOp(ast.Not(), ast.IfExp(
                        ast.Subscript(self.ctx.scope(inner_scope, "exc"), ast.Constant(2, None), None),
                        ast.Call(
                            ast.Attribute(self.ctx.scope(inner_scope, "caught_exc"), "append", None),
                            [ast.Call(
                                ast.Attribute(self.ctx.active_exc(), "extend", None),
                                [self.ctx.scope(inner_scope, "exc")],
                                []
                            )],
                            []
                        ),
                        ast.Constant(None, None)
                    ))
                )
            ])
        ], []), [], []), [ast.Lambda(BLANK_ARGUMENTS, wrapped_body)], []), [], [])
        nodes = [body]
        for handler in handlers:
            inner_handler = ast.Tuple(
                ([
                    ast.Lambda(BLANK_ARGUMENTS, _wrap_listcomp(
                        ast.Constant(None, None),
                        [handler[1]],
                        ast.Subscript(self.ctx.active_exc(), ast.Constant(-2, None), None)
                    ))
                ] if handler[1] else []) + [
                    ast.Lambda(BLANK_ARGUMENTS, expr) for expr in handler[2]
                ] + [
                    ast.Lambda(BLANK_ARGUMENTS, ast.Call(
                        ast.Attribute(self.ctx.scope(inner_scope, "handled_exc"), "append", None),
                        [ast.Constant(None, None)],
                        []
                    )),
                ] + ([
                    ast.Lambda(BLANK_ARGUMENTS, self._delete([handler[1]])[0])
                ] if handler[1] else [])
            )
            wrapped_handler = self._wrap_function(inner_handler, 0, inner_scope, "stop", None, False)
            nodes.append(ast.IfExp(ast.BoolOp(ast.And(), [
                ast.UnaryOp(ast.Not(), self.ctx.scope(inner_scope, "handled_exc")),
                self.ctx.scope(inner_scope, "caught_exc")
            ] + ([] if handler[0] is None else [ast.Call(
                self.ctx.get_builtin("issubclass"),
                [
                    ast.Subscript(self.ctx.active_exc(), ast.Constant(-3, None), None),
                    handler[0]
                ], []
            )])), wrapped_handler, ast.Constant(None, None)))
        if orelse:
            inner_orelse = ast.Tuple([ast.Lambda(BLANK_ARGUMENTS, expr) for expr in orelse])
            wrapped_orelse = self._wrap_function(inner_orelse, 0, inner_scope, "stop", None, False)
            nodes.append(ast.IfExp(
                ast.UnaryOp(ast.Not(), self.ctx.scope(inner_scope, "caught_exc")),
                wrapped_orelse,
                ast.Constant(None, None)
            ))
        if finalbody:
            inner_finally = ast.Tuple([ast.Lambda(BLANK_ARGUMENTS, expr) for expr in finalbody])
            wrapped_finally = self._wrap_function(inner_finally, 0, inner_scope, "stop", None, False)
            nodes.append(wrapped_finally)
        nodes.append(ast.IfExp(
            ast.BoolOp(ast.And(), [
                ast.UnaryOp(ast.Not(), self.ctx.scope(inner_scope, "handled_exc")),
                self.ctx.scope(inner_scope, "caught_exc")
            ]),
            self._raise(ast.Subscript(self.ctx.active_exc(), ast.Constant(-2, None), None), None),
            ast.Constant(None, None)
        ))
        nodes.append(ast.IfExp(self.ctx.scope(inner_scope, "caught_exc"), ast.Call(
            ast.Attribute(self.ctx.active_exc(), "__delitem__", None),
            [ast.Call(
                self.ctx.get_builtin("slice"),
                [ast.Constant(-3, None), ast.Call(self.ctx.get_builtin("len"), [self.ctx.active_exc()], [])],
                []
            )],
            []
        ), ast.Constant(None, None)))
        return _wrap_listcomp(
            ast.Tuple(nodes),
            [ast.Tuple([self.ctx.scope(inner_scope, "handled_exc"), self.ctx.scope(inner_scope, "caught_exc")])],
            ast.Tuple([ast.List([]), ast.List([])])
        )

    def visit_ExceptHandler(self, node: ast.ExceptHandler) -> ast.AST:
        # IMPORTANT: this code must not return None or a list (it must return a single node),
        # otherwise invariants in _try will be false.
        return self.generic_visit(node)

    def visit_Raise(self, node: ast.Raise) -> ast.AST:
        ret = self.generic_visit(node)
        return self._raise(ret.exc, ret.cause)

    def _raise(self, exc: ast.AST = None, cause: ast.AST = None):
        inner_scope = TempScope()
        return _wrap_listcomp(
            ast.Call(
                ast.Attribute(ast.GeneratorExp(ast.Constant(None, None), [
                    ast.comprehension(ast.Name(self.ctx.writeonly(), None), ast.Tuple([]), [], 0)
                ]), "throw", None),
                [self.ctx.scope(inner_scope, "raising_exc")],
                []
            ),
            [ast.Tuple([
                self.ctx.scope(inner_scope, "raising_exc"),
                ast.Attribute(self.ctx.scope(inner_scope, "raising_exc"), "__cause__", None)
            ])],
            ast.Tuple([
                ast.Subscript(_wrap_listcomp(
                    ast.IfExp(
                        ast.Call(
                            self.ctx.get_builtin("isinstance"),
                            [self.ctx.scope(inner_scope, "raising_exc_or_class"), self.ctx.get_builtin("BaseException")],
                            []
                        ),
                        self.ctx.scope(inner_scope, "raising_exc_or_class"),
                        ast.Call(self.ctx.scope(inner_scope, "raising_exc_or_class"), [], [])
                    ),
                    [self.ctx.scope(inner_scope, "raising_exc_or_class")],
                    exc or ast.IfExp(
                        self.ctx.active_exc(),
                        ast.Subscript(self.ctx.active_exc(), ast.Constant(-2, None), None),
                        self._raise(ast.Call(
                            self.ctx.get_builtin("RuntimeError"),
                            [ast.Constant("No active exception to reraise", None)],
                            []
                        ))
                    )
                ), ast.Constant(0, None), None),
                cause or ast.Constant(None, None)
            ])
        )

    def _rewrite_errors(
            self,
            /,
            body: ast.expr,
            preserve_cause: bool = False,
            **errors: tuple[str, typing.Union[str, ast.AST, None]]
    ):
        inner_scope = TempScope()
        handlers = []
        for catch, (throw, message) in errors.items():
            handlers.append((
                self.ctx.get_builtin(catch),
                None,
                [self._raise(
                    ast.Call(self.ctx.get_builtin(throw), [message] if message is not None else [], []),
                    ast.Subscript(self.ctx.active_exc(), ast.Constant(-2, None), None) if preserve_cause else None
                )] if throw else []
            ))
        return ast.Subscript(ast.Subscript(_wrap_listcomp(
            ast.Tuple([
                self._try(
                    [ast.Call(ast.Attribute(self.ctx.scope(inner_scope, "ret"), "append", None), [body], [])],
                    handlers,
                    None,
                    None
                ),
                self.ctx.scope(inner_scope, "ret")
            ]),
            [self.ctx.scope(inner_scope, "ret")],
            ast.List([]),
            True
        ), ast.Constant(1, None), None), ast.Constant(0, None), None)

    visit_ListComp = visit_SetComp = visit_GeneratorExp = DictComp = lambda self, node: self._wrap_locals(
        self.generic_visit(node),
        [self.ctx.scope_from_node(comprehension) for comprehension in node.generators]
    )

    def visit_comprehension(self, node: ast.comprehension) -> ast.comprehension:
        target = self.unpack_visit(node, "target")
        scope = self.scope.pop()
        iter_expr = self.unpack_visit(node, "iter")
        self.scope.append(scope)
        ifs = self.unpack_visit(node, "ifs")
        return ast.comprehension(target, iter_expr, ifs, node.is_async)

    def visit_Lambda(self, node: ast.Lambda) -> ast.AST:
        ret = self.generic_visit(node)
        inner_body_header = self.ctx.init_scope(self.scope[-1])
        inner_body = ast.Tuple(inner_body_header + [ret.body])

        body = ast.Subscript(self._wrap_locals(inner_body, [self.scope[-1]]), ast.Constant(-1, None), None)
        return ast.Lambda(ret.args, body)

    def visit_FunctionDef(self, node: ast.FunctionDef) -> ast.AST:
        self.stop_tests[-1].append(StopConditionAll(self.scope[-1], "stop"))
        ret = self.generic_visit(node)
        self.stop_tests[-1].pop()
        inner_body_header = self.ctx.init_scope(self.scope[-1])
        inner_body = ast.Tuple(inner_body_header + [
            ast.Lambda(BLANK_ARGUMENTS, ast.Tuple([self.ctx.scope(self.scope[-1], "stop"), expr])) for expr in ret.body
        ])

        body = ast.Lambda(ret.args, ast.Subscript(ast.BoolOp(ast.Or(), [
            self._wrap_function(inner_body, len(inner_body_header), self.scope[-1]),
            ast.List([ast.Constant(None, None)])
        ]), ast.Constant(0, None), None))
        for decorator in reversed(ret.decorator_list):
            body = ast.Call(decorator, [body], [])

        return self.ctx.assign_var_in_scope(self.scope[-2], ret.name, body)

    def visit_Return(self, node: ast.Return) -> ast.AST:
        return self._stop(StopCause.RETURN, self.visit(node.value))

    def visit_ClassDef(self, node: ast.ClassDef) -> ast.AST:
        ret = self.generic_visit(node)

        inner_body = ast.Tuple([ClassCellAddingTransformer(self.ctx, ast.Subscript(
            self.ctx.scope(self.scope[-1], "instantiated"),
            ast.Constant(0, None),
            None
        )).visit(expr) for expr in ret.body])

        metaclass_invoker = ast.Call(ast.Attribute(ast.Name(self.ctx.name("_types"), None), "new_class", None), [
            ast.Constant(ret.name, None),
            ast.Tuple(ret.bases),
            ast.Dict([ast.Constant(keyword.arg, None) for keyword in ret.keywords], [keyword.value for keyword in ret.keywords]),
            ast.Lambda(
                ast.arguments([], [ast.arg(self.ctx.scope_name(self.scope[-1], "ns"), None, None)], None, [], [], None, []),
                ast.Subscript(ast.Tuple([
                    self._wrap_locals(inner_body, [self.scope[-1]], self.ctx.scope(self.scope[-1], "ns")),
                    self.ctx.scope(self.scope[-1], "ns")
                ]), ast.Constant(-1, None), None)
            )
        ], [])

        body = _wrap_listcomp(
            _wrap_listcomp(
                ast.Constant(None, None),
                [ast.Subscript(
                    self.ctx.scope(self.scope[-1], "instantiated"),
                    ast.Constant(0, None),
                    None
                ), self.ctx.var(ret.name, self.scope[-2])],
                metaclass_invoker
            ),
            [self.ctx.scope(self.scope[-1], "instantiated")],
            ast.List([ast.List([])])
        )
        for decorator in reversed(ret.decorator_list):
            body = ast.Call(decorator, [body], [])
        return body

    def _wrap_function(
            self,
            inner_body: ast.Tuple,
            inner_body_header_length: int,
            scope: ScopeLike,
            stop_name: typing.Optional[str] = "stop",
            data_type: typing.Optional[ast.AST] = ast.Dict([], []),
            subscript: bool = True
    ):
        ret = ast.ListComp(
            ast.Call(ast.Subscript(inner_body, self.ctx.scope(scope, "i"), None), [], []),
            [ast.comprehension(
                ast.Tuple([
                    self.ctx.scope(scope, "i"),
                    *((self.ctx.scope(scope, stop_name),) if stop_name else ()),
                    *((self.ctx.scope(scope, "data"),) if data_type else ())
                ], None),
                ast.Call(ast.Name("zip", None), [
                    ast.Call(
                        ast.Name("range", None),
                        [ast.Constant(inner_body_header_length, None), ast.Constant(len(inner_body.elts), None)],
                        []
                    ),
                    *((ast.BinOp(
                        ast.Tuple([ast.List([])]),
                        ast.Mult(),
                        ast.Constant(len(inner_body.elts) - inner_body_header_length, None)
                    ),) if stop_name else ()),
                    *((ast.BinOp(
                        ast.Tuple([data_type]),
                        ast.Mult(),
                        ast.Constant(len(inner_body.elts) - inner_body_header_length, None)
                    ),) if data_type else ())
                ], []),
                [ast.UnaryOp(ast.Not(), self.ctx.scope(scope, stop_name))] if stop_name else [],
                0  # TODO async stuff?
            )]
        )
        if subscript:
            ret = ast.Subscript(
                ast.Subscript(ret, ast.Constant(0, None), None),
                ast.Constant(0, None),
                None
            )
        return ret

    def _wrap_locals(self, inner_body: ast.AST, scopes: list[ScopeLike], data_type: ast.AST = ast.Dict([], [])):
        data = [self.ctx.scope(scope, "data") for scope in scopes]
        return ast.Subscript(ast.ListComp(inner_body, [ast.comprehension(
            data[0] if len(data) == 1 else ast.Tuple([data]),
            ast.Tuple([data_type]) if len(data) == 1 else ast.Tuple([ast.Tuple(data_type)] * len(data)),
            [],
            0
        )]), ast.Constant(0, None), None)

    def _wrap_loop(
            self,
            inner_scope: ScopeLike,
            inner_body: ast.Tuple,
            iterator: ast.AST,
            target: ast.AST,
            test: typing.Optional[ast.AST]
    ):
        return ast.ListComp(
            self._wrap_function(inner_body, 0, inner_scope, "continue", None, False) if test is None else ast.IfExp(
                test,
                self._wrap_function(inner_body, 0, inner_scope, "continue", None, False),
                ast.Call(
                    ast.Attribute(self.ctx.scope(inner_scope, "break"), "append", None),
                    [ast.Constant(None, None)],
                    []
                )
            ),
            [ast.comprehension(
                    target,
                    iterator,
                    [ast.UnaryOp(ast.Not(), self.ctx.scope(inner_scope, "break"))] if test else [],
                    0  # TODO: async
            )]
        )

    def _infinite_iter(self, until: ast.AST = None):
        return ast.Call(ast.Call(self.ctx.get_builtin("type"), [
            ast.Constant("", None),
            ast.Constant((), None),
            ast.Dict([ast.Constant("__getitem__", None)], [ast.Lambda(
                ast.arguments(
                    [],
                    [ast.arg(self.ctx.writeonly(0), None), ast.arg(self.ctx.writeonly(1), None)],
                    None,
                    [],
                    [],
                    None,
                    []
                ),
                ast.Constant(None, None) if until is None else ast.IfExp(
                    until,
                    ast.Subscript(ast.List([]), ast.Constant(0, None), None),
                    ast.Constant(None, None)
                )
            )])
        ], []), [], [])
