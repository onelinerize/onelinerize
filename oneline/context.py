import ast
import itertools
import keyword
import string
import typing

import ast_scope.utils
from ast_scope.scope import Scope, ScopeWithChildren, FunctionScope, ClassScope, GlobalScope

from .temp_scope import TempScope
from .utils import ScopeLike, ScopeType


class Context:
    def __init__(self, tree: ast.Module, shorten_names: bool = False, debug: bool = False):
        self.ast = tree
        self.scopes = ast_scope.annotate(tree)
        self.all_scopes = get_all_scopes(self.scopes.global_scope)
        self.all_symbols = set(get_all_symbols(self.scopes.global_scope))
        self.shorten_names = shorten_names
        self.debug = debug
        if debug and shorten_names:
            raise RuntimeError("cannot debug while shortening names")
        if shorten_names:
            self.names = {}
            self.valid_characters = string.ascii_letters + string.digits
            self.name_gen = None
            self.name_length = 0

    @property
    def global_scope(self):
        return self.scopes.global_scope

    def var(self, name: typing.Union[ast.Name, str], scope: ScopeLike = None):
        if isinstance(name, ast.Name):
            scope = scope or name
            name = name.id
        elif scope is None:
            raise RuntimeError("scope is None and cannot be inferred")
        return ast.Subscript(self.scope(scope, "data"), ast.Index(ast.Constant(name, None)), None)

    def scope_from_node(self, node: ast.AST) -> typing.Optional[ScopeType]:
        for scope in self.all_scopes:
            if isinstance(scope, FunctionScope) and scope.function_node is node:
                return scope
            if isinstance(scope, ClassScope) and scope.class_node is node:
                return scope
        try:
            return self.scopes[node]
        except KeyError:
            return None

    def scope_name(self, scope: ScopeLike, var_type: str) -> typing.Optional[str]:
        scope_type = None
        scope_id = None
        if isinstance(scope, ast.AST):
            new_scope = self.scope_from_node(scope)
        else:
            new_scope = scope
        if isinstance(new_scope, Scope):
            scope_type = "s"
            scope_id = self.all_scopes.index(new_scope)
        elif isinstance(new_scope, TempScope):
            scope_type = "t"
            scope_id = new_scope.counter
        if scope_type is None or scope_id is None:
            raise TypeError("Unable to determine type or id")
        if self.debug and isinstance(new_scope, Scope):
            if isinstance(new_scope, GlobalScope):
                name = "g"
            elif isinstance(new_scope, FunctionScope):
                try:
                    name = "f" + new_scope.function_node.name
                except AttributeError:
                    name = "uf"
            elif isinstance(new_scope, ClassScope):
                try:
                    name = "c" + new_scope.class_node.name
                except AttributeError:
                    name = "uc"
            else:
                name = "u"
            debug = f"_d{name}"
        else:
            debug = ""
        return self.name(f"_scope_{var_type}_{scope_type}{scope_id}{debug}")

    def scope(self, scope: ScopeLike, name: str) -> ast.AST:
        return ast.Name(self.scope_name(scope, name), None)

    def init_scope(self, scope: Scope) -> list[ast.AST]:
        if scope is self.global_scope:
            return [
                ast.Call(
                    ast.Attribute(ast.Call(ast.Name("globals", None), [], []), "__setitem__", None),
                    [
                        ast.Constant(self.scope_name(scope, "data"), None),
                        ast.Dict([None, None], [
                            ast.Attribute(
                                ast.Call(ast.Name("__import__", None), [ast.Constant("builtins", None)], []),
                                "__dict__",
                                None
                            ),
                            ast.Call(ast.Name("globals", None), [], [])
                        ]),
                    ],
                    []
                ),
                ast.Call(
                    ast.Attribute(ast.Call(ast.Name("globals", None), [], []), "__setitem__", None),
                    [ast.Constant(self.name("_builtins"), None), ast.Call(ast.Attribute(
                        ast.Attribute(ast.Call(ast.Name("__import__", None), [ast.Constant("builtins", None)], []), "__dict__", None),
                        "copy",
                        None
                    ), [], [])],
                    []
                ),
                ast.Call(
                    ast.Attribute(ast.Call(ast.Name("globals", None), [], []), "__setitem__", None),
                    [ast.Constant(self.scope_name(self.global_scope, "active_exc"), None), ast.Call(ast.Attribute(ast.Call(ast.Name("__import__", None), [ast.Constant("threading", None)], []), "local", None), [], [])],
                    []
                ),
                ast.Call(
                    ast.Attribute(self.scope(self.global_scope, "active_exc"), "__setattr__", None),
                    [ast.Constant("exc", None), ast.List([])],
                    []
                ),
                ast.Call(
                    ast.Attribute(ast.Call(ast.Name("globals", None), [], []), "__setitem__", None),
                    [ast.Constant(self.name("_sentinel"), None), ast.Call(ast.Name("object", None), [], [])],
                    []
                ),
                ast.Call(
                    ast.Attribute(ast.Call(ast.Name("globals", None), [], []), "__setitem__", None),
                    [ast.Constant(self.name("_types"), None), ast.Call(ast.Name("__import__", None), [ast.Constant("types", None)], [])],
                    []
                ),
            ]
        elif isinstance(scope, FunctionScope):
            arguments = scope.function_node.args
            args = arguments.posonlyargs + arguments.args + arguments.kwonlyargs
            if arguments.kwarg is not None:
                args.append(arguments.kwarg)
            if arguments.vararg is not None:
                args.append(arguments.vararg)
            return [
                self.assign_var_in_scope(scope, arg.arg, ast.Name(arg.arg, None))
                for arg in args
            ]

    def active_exc(self) -> ast.AST:
        return ast.Attribute(self.scope(self.global_scope, "active_exc"), "exc", None)

    def name(self, name: str) -> str:
        if self.shorten_names:
            if name in self.names:
                return self.names[name]
            new_name = ""
            while not new_name.isidentifier() or new_name in self.all_symbols or keyword.iskeyword(new_name):
                if self.name_gen is None:
                    self.name_length += 1
                    self.name_gen = iter(itertools.product(self.valid_characters, repeat=self.name_length))
                try:
                    new_name = "".join(next(self.name_gen))
                except StopIteration:
                    self.name_gen = None
            self.names[name] = new_name
        else:
            new_name = name
            while new_name in self.all_symbols or keyword.iskeyword(new_name):
                new_name += "_"
        return new_name

    def parent(self, node: ast.AST, _start=None) -> ast.AST:
        for child in ast.iter_child_nodes(_start or self.ast):
            if child is node:
                return _start or self.ast
            self.parent(node, child)

    def assign_var_in_scope(self, scope: ScopeLike, name: str, value: ast.AST) -> ast.AST:
        return ast.Call(
            ast.Attribute(self.scope(scope, "data"), "__setitem__", None),
            [ast.Constant(name, None), value], []
        )

    def get_builtin(self, name: str) -> ast.AST:
        return ast.Subscript(ast.Name(self.name("_builtins"), None), ast.Constant(name, None), None)

    def writeonly(self, separate: int = 0) -> str:
        return self.name(f"_writeonly_{separate}" if separate else "_writeonly")

    def sentinel(self):
        return ast.Name(self.name("_sentinel"), None)

    def store_scope(self, scope: ScopeLike):
        if scope is self.global_scope:
            return [ast.Call(ast.Attribute(ast.Call(self.get_builtin("globals"), [], []), "update", None), [self.scope(scope, "data")], [])]


def get_all_symbols(scope: Scope):
    ret = scope.variables.all_symbols
    if isinstance(scope, ScopeWithChildren):
        for child in scope.children:
            ret |= get_all_symbols(child)
    return ret


def get_all_scopes(scope: Scope):
    ret = [scope]
    if isinstance(scope, ScopeWithChildren):
        for child in scope.children:
            ret += get_all_scopes(child)
    return ret
