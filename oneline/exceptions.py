import ast
import typing

T = typing.TypeVar("T", bound=ast.AST)


class ErrorWithLocation(Exception):
    def __init__(self, node: typing.Any, stack: list[ast.AST]):
        while node is None:
            try:
                node = stack.pop()
            except IndexError:
                break
        location_node = node
        while True:
            try:
                lineno = location_node.lineno
                col_offset = location_node.col_offset
            except AttributeError:
                try:
                    location_node = stack.pop()
                except IndexError:
                    lineno = "?"
                    col_offset = "?"
                    break
            else:
                break
        try:
            super().__init__(f"error occurred around {lineno}:{col_offset} = {ast.dump(node)}")
        except AttributeError:
            super().__init__(f"error occurred around {lineno}:{col_offset} = {node}")
        self.node = node
        self.stack = stack
