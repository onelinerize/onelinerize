import ast


class LocationRemovingTransformer(ast.NodeTransformer):
    def visit(self, node):
        super().visit(node)
        node.lineno = None
        node.col_offset = None
        node.end_lineno = None
        node.end_col_offset = None
        return node
