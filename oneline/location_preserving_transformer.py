import ast
import itertools
import typing

T = typing.TypeVar("T", bound=ast.AST)


class LocationPreservingTransformer(ast.NodeTransformer):
    def visit(self, node: T) -> T:
        return self.fix_location(node, super().visit(node))

    def generic_visit(self, node: T) -> T:
        return self.fix_location(node, super().generic_visit(node))

    def unpack_visit(self, node: T, field: str):
        # noinspection PyUnresolvedReferences
        return self.fix_location(node, super().unpack_visit(node, field))

    def fix_location(self, node, ret):
        if isinstance(ret, ast.AST):
            nodes = ast.walk(ret)
        elif isinstance(ret, list):
            nodes = itertools.chain(ast.walk(inner) for inner in ret)
        else:
            return ret
        for child in nodes:
            self.fix_single_location(node, child)
        return ret

    def fix_single_location(self, node, ret):
        try:
            ret.lineno = getattr(ret, "lineno", None) or getattr(node, "lineno", 0)
        except AttributeError:
            pass
        try:
            ret.col_offset = getattr(ret, "col_offset", None) or getattr(node, "col_offset", 0)
        except AttributeError:
            pass
        try:
            ret.end_lineno = getattr(ret, "end_lineno", None) or getattr(node, "end_lineno", 0)
        except AttributeError:
            pass
        try:
            ret.end_col_offset = getattr(ret, "end_col_offset", None) or getattr(node, "end_col_offset", 0)
        except AttributeError:
            pass
