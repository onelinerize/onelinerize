import ast

from .transformer import transform


def main():
    d = open("oneline/main_transformer.py").read()
    #d = open("input.py").read()
    t = ast.parse(d)
    print(ast.dump(t))
    t = transform(t, True, False)
    print(ast.dump(t))
    s = ast.unparse(t)
    print(s)
    exec(compile(t, "input.py", "exec"), {}, {})


if __name__ == "__main__":
    main()
