from enum import IntEnum


class StopCause(IntEnum):
    RETURN = 0
    BREAK = 1
    CONTINUE = 2


class StopCondition:
    CAUSES = ()
    STORE_VALUE = False

    def __init__(self, scope, name):
        self.scope = scope
        self.name = name

    def handle(self, cause):
        return cause in self.CAUSES

    def stop_handling(self, cause):
        return False

    def var(self, context):
        return context.scope(self.scope, self.name)


class StopConditionAll(StopCondition):
    CAUSES = (StopCause.RETURN,)
    STORE_VALUE = True


class StopConditionBreak(StopCondition):
    CAUSES = (StopCause.RETURN, StopCause.BREAK)


class StopConditionContinue(StopCondition):
    CAUSES = (StopCause.RETURN, StopCause.BREAK, StopCause.CONTINUE)


class StopConditionIf(StopCondition):
    CAUSES = (StopCause.RETURN, StopCause.BREAK, StopCause.CONTINUE)


class StopConditionTry(StopCondition):
    CAUSES = (StopCause.RETURN, StopCause.BREAK, StopCause.CONTINUE)


class StopConditionLoopMarker(StopCondition):
    def stop_handling(self, cause):
        return cause in (StopCause.BREAK, StopCause.CONTINUE)
